<div class="col-xs-12 col-sm-4">
    <aside>
        <?php if ( is_active_sidebar( 'main' ) ) : ?>
            <?php dynamic_sidebar( 'main' ); ?>
        <?php endif; ?>
    </aside>
</div>