<?php get_header(); ?>
<?php get_template_part( 'slider' ); ?>
<main class="content">
	<div class="row">
		<?php get_template_part( '/inc/parts/content', 'index' ); ?>
    <?php get_sidebar(); ?>
 </div>
</main>
<?php get_footer(); ?>