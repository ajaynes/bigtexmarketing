<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>
<div class="jumbotron">
  <div class="container">
  	<?php the_field('hero_image_content'); ?>
  </div>
</div>
<div class="container">  	
<main class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'home' ); ?>
  </div><!--row-->
	<?php $queryBT = new WP_Query(array( 'cat'=>'3', 'posts_per_page'=>-1 )); ?>
  <?php if ($queryBT->have_posts()) : ?>
    <?php while ( $queryBT->have_posts() ) : $queryBT->the_post(); ?>
      <div class="col-sm-4 services">
        <section class="service">
        	<div class="row">
          	<div class="col-sm-3 col-sm-offset-4">
            	<?php if ( has_post_thumbnail() ) { the_post_thumbnail('full', array('class' => 'img-responsive aligncenter')); } ?>
            </div>
          </div>
          <h3 class="text-center"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
          <?php the_excerpt(); ?>
          <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>" class="excerpt-more">Learn More</a></p>
        </section>
      </div>
    <?php endwhile; // end of the loop. ?>
    <?php else : ?>
      <section class="post">
        <h3>Not Found</h3>
        <p>Sorry, but the requested resource was not found on this site.</p>
      </section>
  <?php endif; ?>
</main><!--content-->
</div>
<?php get_template_part( 'casestudies' ); ?>
<?php get_template_part( 'contactinfo' ); ?>
<?php get_template_part( 'newsletter' ); ?>
<div class="container">
<?php get_footer(); ?>