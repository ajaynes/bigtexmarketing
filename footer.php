</div><!--container--> 
<footer>
<div class="container">
    <div class="row">
      <div class="col-xs-12 text-center">
        <?php wp_nav_menu(array(
						'theme_location' => 'footer',
						'container' => 'nav',
						'menu_class' => 'list-inline list-unstyled',
						'fallback_cb' => false
					));
				?>
      </div>
    </div> 
  </div><!--container--> 
</footer>
<?php wp_footer(); ?>
</body></html>