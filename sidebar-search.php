<div class="col-xs-12 col-sm-4 col-sm-pull-8">
    <aside>
        <?php if ( is_active_sidebar( 'search' ) ) : ?>
            <?php dynamic_sidebar( 'search' ); ?>
        <?php endif; ?>
    </aside>
</div>