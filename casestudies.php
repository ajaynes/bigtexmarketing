<div class="casestudies">
  <div class="container">
  	<div class="row">
      <div class="col-sm-5">
      <h2 class="leftunderline">Case Studies</h2>
      <p>We are well aware of how difficult it is to stand out from the crowd, especially with the amount of information, new companies and products offered to us every day. This is why, above all, we focus on developing your visual brand communication is such a way as to draw...</p>
      <ol class="carousel-indicators">
        <li data-target="#cases" data-slide-to="0"></li>
        <li data-target="#cases" data-slide-to="1"></li>
        <li data-target="#cases" data-slide-to="2"></li>
      </ol>
      </div><!--col-sm-5-->
      <div class="col-sm-6 col-sm-offset-1">
        <div id="cases" class="carousel slide" data-ride="carousel">
        <div class="row">
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <div class="col-sm-6">
                <div class="study">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/casestudy1.jpg" class="img-responsive" alt="Case Study 1" />
                  <div class="carousel-caption">
                    <p class="case-title">Uber</p>
                    <p class="case-summary">Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
                    <p><a class="excerpt-more" href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="study">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/casestudy2.jpg" class="img-responsive" alt="Case Study 2" />
                  <div class="carousel-caption">
                  <p class="case-title">AirBNB</p>
                  <p class="case-summary">Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
                  <p><a class="excerpt-more" href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
            </div><!--item active-->
            <div class="item">
              <div class="col-sm-6">
                <div class="study">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/casestudy3.jpg" class="img-responsive" alt="Case Study 3" />
                  <div class="carousel-caption">
                  <p class="case-title">Category</p>
                  <p class="case-summary">Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
                  <p><a class="excerpt-more" href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="study">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/casestudy1.jpg" class="img-responsive" alt="Case Study 1" />
                  <div class="carousel-caption">
                  <p class="case-title">Uber</p>
                  <p class="case-summary">Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
                  <p><a class="excerpt-more" href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
            </div><!--item-->
            <div class="item">
              <div class="col-sm-6">
                <div class="study">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/casestudy2.jpg" class="img-responsive" alt="Case Study 2" />
                  <div class="carousel-caption">
                  <p class="case-title">AirBNB</p>
                  <p class="case-summary">Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
                  <p><a class="excerpt-more" href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="study">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/casestudy3.jpg" class="img-responsive" alt="Case Study 3" />
                  <div class="carousel-caption">
                  <p class="case-title">Category</p>
                  <p class="case-summary">Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
                  <p><a class="excerpt-more" href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
            </div><!--item-->
          </div><!--carousel-inner-->
          </div><!--row-->
        </div><!--carousel-->
      </div><!--col-sm-7-->
    </div><!--row-->
  </div><!--container-->
</div><!--casestudies-->