<?php /*?><div class="circle"></div>
<nav class="navbar navbar-default navbar-fixed-top bt transparent">
	<div class="container">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bt-collapse">
      <div class="sr-only">Toggle navigation</div>
      <div class="x"></div>
      <div class="y"></div>
      <div class="z"></div>
    </button>
    <a class="navbar-brand logo-light" href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_directory'); ?>/images/logo-small.png" class="img-responsive"></a>
    <a class="navbar-brand logo-dark" href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_directory'); ?>/images/logo-dark.png" class="img-responsive"></a>
  </div>
    <?php wp_nav_menu( array(
        'menu'      		=> 'main',
				'theme_location' => 'main',
        'depth'     		=> 2,
        'container'  		=> 'div',
        'container_class'   => 'collapse navbar-collapse',
        'container_id'      => 'bt-collapse',
        'menu_class'        => 'nav navbar-nav navbar-right bt',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker())
      );
    ?>
	</div>
</nav><?php */?>
<nav class="navbar navbar-default navbar-fixed-top bt transparent">
	<div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bt-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand logo-light" href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_directory'); ?>/images/logo-small.png" class="img-responsive"></a>
    	<a class="navbar-brand logo-dark" href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_directory'); ?>/images/logo-dark.png" class="img-responsive"></a>
    </div>
      <?php wp_nav_menu( array(
          'menu'      		=> 'main',
          'theme_location' => 'main',
          'depth'     		=> 2,
          'container'  		=> 'div',
          'container_class'   => 'collapse navbar-collapse',
          'container_id'      => 'bt-collapse',
          'menu_class'        => 'nav navbar-nav navbar-right bt',
          'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
          'walker'            => new wp_bootstrap_navwalker())
        );
      ?>
	</div>
</nav>