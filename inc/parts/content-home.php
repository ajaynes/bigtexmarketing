<div class="col-xs-12">
    <article>
      <h1 class="text-center"><?php the_title(); ?></h1>
      <div class="row">
      	<div class="col-sm-6 col-sm-offset-3">
      		<div class="text-center"><?php the_field('lead_paragraph'); ?></div>
        </div>
      </div>
      <?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
        <?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
      <?php endwhile; // end of the loop. ?>
    </article>
</div><!--col-xs-8-->