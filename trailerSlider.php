<div class="col-xs-12">
	<h3 class="trailerslider">Trailer Styles</h3>
  <div class="trailerSlide">
    <div class="carousel slide" id="trailer">
      <div class="row">
        <div class="carousel-inner">
          <div class="item active">
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/big-tex-automotorcycle-haulers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/autohauler.png" class="img-responsive" alt="Auto Hauler" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/big-tex-automotorcycle-haulers/">Auto Haulers</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/cm-truck-beds/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/cmtruckbeds.png" class="img-responsive" alt="CM Truck Beds" />
                </a>
              </div><!--thumbnail bt-->
            	<div class="caption">
                <a href="/cm-truck-beds/">CM Truck Beds</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/big-tex-gooseneck-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/gooseneck.png" class="img-responsive" alt="Gooseneck Trailer" />
                </a>
              </div><!--thumbnail bt-->
             	<div class="caption">
                <a href="/big-tex-gooseneck-trailers/">Goosenecks</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/cm-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/cmtrailers.png" class="img-responsive" alt="CM Trailers" />
                </a>  
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/cm-trailers/">CM Trailers</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
          </div><!--item-->
          <div class="item">
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/big-tex-single-axle-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/singleaxle.png" class="img-responsive" alt="Enclosed Trailer" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/big-tex-single-axle-trailers/">Single Axles</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/big-tex-landscape-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/landscape.png" class="img-responsive" alt="Landscape Trailer" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/big-tex-landscape-trailers/">Landscape</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/big-tex-dump-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/dump.png" class="img-responsive" alt="Dump Trailer" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/big-tex-dump-trailers/">Dumps</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/big-tex-tandem-axle-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/tandemaxle.png" class="img-responsive" alt="Tandem Axle Trailer" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/big-tex-tandem-axle-trailers/">Tandem Axles</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
          </div><!--item2-->
          <div class="item">
            
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/big-tex-heavy-equipment-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heavyequipment.png" class="img-responsive" alt="Heavy Equipment" />
                </a>  
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/big-tex-heavy-equipment-trailers/">Equipment</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/big-tex-automotorcycle-haulers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/autohauler.png" class="img-responsive" alt="Auto Hauler" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/big-tex-automotorcycle-haulers/">Auto Haulers</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/big-tex-single-axle-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/singleaxle.png" class="img-responsive" alt="Enclosed Trailer" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/big-tex-single-axle-trailers/">Single Axles</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
            <div class="col-xs-12 col-sm-3">
              <div class="thumbnail bt">
                <a href="/big-tex-gooseneck-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/gooseneck.png" class="img-responsive" alt="Gooseneck Trailer" />
                </a>
              </div><!--thumbnail bt-->
             	<div class="caption">
                <a href="/big-tex-gooseneck-trailers/">Goosenecks</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-3-->
          </div><!--item-->     
        </div><!--carousel-inner-->
      </div><!--row-->
      <a class="left carousel-control hidden-xs bt" href="#trailer" data-slide="prev">
    		<span class="fa-stack left">
        	<span class="fa fa-square fa-stack-2x"></span>
        	<span class="fa fa-chevron-left fa-stack-1x fa-inverse"></span>
        </span>
  		</a>
  		<a class="right carousel-control hidden-xs bt" href="#trailer" data-slide="next">
      	<span class="fa-stack right">
        	<span class="fa fa-square fa-stack-2x"></span>
    			<span class="fa fa-chevron-right fa-stack-1x fa-inverse"></span>
        </span>
  		</a>
    </div><!--carousel slide-->
  </div><!--trailerSlide-->
</div><!--col-xs-12-->