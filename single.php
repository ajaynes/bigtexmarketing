<?php get_header(); ?>
<main class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'single' ); ?>
    <?php get_sidebar(); ?>
  </div>
</main>
<?php get_footer(); ?>