<!DOCTYPE html>
<html <?php language_attributes(); ?><?php if ( wp_is_mobile() ){echo 'class="mobile"';}?>>
		<!--[if lte IE 8 ]> <html <?php language_attributes(); ?> class="ie8"> <![endif]--><head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
		<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><!--add comment before this meta tag if this site is to be fixed-->
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
    <a href="#" class="go-top hidden-xs hidden-sm"><span class="fa fa-arrow-circle-o-up fa-2x fa-fw"></span></a>
		<header>
    	<?php get_template_part( '/inc/parts/menu', 'nav' ); ?>
		</header>