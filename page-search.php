<?php
/*
Template Name: Inventory Search
*/
?>
<?php get_header(); ?>
<main class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'page' ); ?>
    <?php get_sidebar('search'); ?>
  </div><!--row-->
</main><!--content-->
<?php get_footer(); ?>