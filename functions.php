<?php
//require plugins
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
function my_theme_register_required_plugins() {
    $plugins = array(
				array(
            'name'      => 'Image Widget',
            'slug'      => 'image-widget',
            'required'  => true,
        ),
				array(
            'name'      => 'Wordpress SEO',
            'slug'      => 'wordpress-seo',
            'required'  => false,
        ),
				array(
            'name'      => 'Google Analytics',
            'slug'      => 'google-analytics-for-wordpress',
            'required'  => false,
        ),
				array(
            'name'      => 'Wordfence',
            'slug'      => 'wordfence',
            'required'  => false,
        ),
				array(
            'name'      => 'Jetpack',
            'slug'      => 'jetpack',
            'required'  => false,
        )
			);
			$config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'tgmpa' ),
            'menu_title'                      => __( 'Install Plugins', 'tgmpa' ),
            'installing'                      => __( 'Installing Plugin: %s', 'tgmpa' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'tgmpa' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins' ),
            'return'                          => __( 'Return to Required Plugins Installer', 'tgmpa' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'tgmpa' ),
            'complete'                        => __( 'All plugins installed and activated successfully. %s', 'tgmpa' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );
    tgmpa( $plugins, $config );
}
//make text widgets execute shortcodes (this is mostly used for trailershopper's search code)
add_filter('widget_text', 'do_shortcode');
//enable featured thumbnails
add_theme_support( 'post-thumbnails' );
//Loads the Theme Options Panel 
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
require_once dirname( __FILE__ ) . '/inc/options-framework.php';
require_once get_template_directory() . '/options.php';
//custom dashboard widget
add_action('wp_dashboard_setup', 'custom_dashboard_widgets');
function custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_widget', 'Recent Website Updates', 'custom_dashboard_widget');
}
function custom_dashboard_widget() {
	echo of_get_option('updates');
}
//add class "img-responsive" to all images that are inserted into pages and posts
function add_image_class($class){
    $class .= ' img-responsive';
    return $class;
}
add_filter('get_image_tag_class','add_image_class');
//Register sidebars
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'id' => 'main',
		'name' => 'Main Sidebar',
		'description' => 'This is the main sidebar',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));	
}
//register navigation
register_nav_menus( array(
 		'main' => __( 'Main Menu', 'ajbootstrap' ),
		'social' => __( 'Social Media Menu', 'ajbootstrap' ),
		'footer' => __( 'Footer Menu', 'ajbootstrap' )
 	) );
// Custom Backend Footer
function bigtex_custom_admin_footer() {
	echo '<span id="footer-thankyou">Developed by <a href="http://bigtextrailers.com" target="_blank">Big Tex Trailers Marketing Team</a></span>.';
}
// adding it to the admin area
add_filter('admin_footer_text', 'bigtex_custom_admin_footer');
// Register Custom Bootstrap Navigation Walker
require_once('wp_bootstrap_navwalker.php');
//enqueue styles and javascript
function stylin(){
	wp_enqueue_style('main', get_bloginfo( 'stylesheet_url', array(), '20160609', 'screen') );
	wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', '', null, 'screen');
	wp_enqueue_style('fontawesome', get_template_directory_uri().'/css/font-awesome.min.css', '', '', 'screen');
	wp_enqueue_style('hover', get_template_directory_uri().'/css/hover-min.css', '', '', 'screen');
}
add_action( 'wp_enqueue_scripts', 'stylin' );
function scriptaculous(){
	wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/javascript/bootstrap.min.js', '', '', true);
	wp_enqueue_script('custom', get_template_directory_uri().'/javascript/custom.js', '', '', true);
}
add_action( 'wp_enqueue_scripts', 'scriptaculous' );
// add ie conditional html5 shim to header
function add_ie_html5_shim () {
    echo '<!--[if lt IE 9]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
		echo '<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>';
    echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');
	//login logo on wp-login.php page. 200px x 100px png named site-login-logo.png in the images folder. Please create.
	function my_login_logo() { ?>
    <style type="text/css">
      body.login div#login h1 a{
			background-image:url(<?php echo get_bloginfo( 'template_directory' ) ?>/images/site-login-logo.png);
			background-size:200px 100px;
			height:100px;
      padding-bottom:30px;
			width:200px;
      }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
//return blog url when clicking on the login logo on wp-login.php
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );
//the title that appears when hovering over the login logo on wp-login.php. CHANGE THIS TO SUIT EACH DEALER
function my_login_logo_url_title() {
    return 'Big Tex Marketing Department';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
//Favicon 16x16 png named favicon.png in images folder is the site's favicon. Please create.
function theme_favicon() { ?>
	<link rel="shortcut icon" href="<?php echo bloginfo('stylesheet_directory') ?>/images/favicon.png" > 
<?php }
add_action('wp_head', 'theme_favicon');
// customize the wordpress visual editor
function my_mce_buttons( $buttons ){
	unset($buttons[7]);
	unset($buttons[8]);
	unset($buttons[9]);
  return $buttons;
}
add_filter('mce_buttons', 'my_mce_buttons');
//edit button on post and pages
function custom_edit_post_link($output) {
 $output = str_replace('class="post-edit-link"', 'class="post-edit-link edit-link btn btn-default btn-block uppercase"', $output);
 return $output;
}
add_filter('edit_post_link', 'custom_edit_post_link');
//Plugin Name: Custom Styles
//Plugin URI: http://www.speckygeek.com
//Description: Add custom styles in your posts and pages content using TinyMCE WYSIWYG editor. The plugin adds a Styles dropdown menu in the visual post editor.
//Based on TinyMCE Kit plug-in for WordPress
//http://plugins.svn.wordpress.org/tinymce-advanced/branches/tinymce-kit/tinymce-kit.php
///** Apply styles to the visual editor*/ 
add_filter('mce_css', 'tuts_mcekit_editor_style');
function tuts_mcekit_editor_style($url) {
	if ( !empty($url) )
			$url .= ',';
	// Retrieves the plugin directory URL
	// Change the path here if using different directories
	$url .= get_stylesheet_directory_uri() . '/css/editor-styles.css';
	return $url;
} 
add_filter( 'mce_buttons_2', 'tuts_mce_editor_buttons' ); 
function tuts_mce_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
add_filter( 'tiny_mce_before_init', 'tuts_mce_before_init' );
function tuts_mce_before_init( $settings ) {
	$style_formats = array(
		array(
			'title' => 'text-left',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'text-left'
		),
		array(
			'title' => 'text-right',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'text-right'
		),
		array(
			'title' => 'text-center',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'text-center'
		),
		array(
			'title' => 'text-justify',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'text-justify'
		),
		array(
			'title' => 'text-muted',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-muted',
		),
		array(
			'title' => 'text-primary',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-primary',
		),
		array(
			'title' => 'text-success',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-success',
		),
		array(
			'title' => 'text-info',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-info',
		),
		array(
			'title' => 'text-warning',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-warning',
		),
		array(
			'title' => 'text-danger',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-danger',
		),
		array(
			'title' => 'bg-primary',
			'selector' => 'p, a, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'bg-primary',
		),
		array(
			'title' => 'bg-success',
			'selector' => 'p, a, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'bg-success',
		),
		array(
			'title' => 'bg-info',
			'selector' => 'p, a, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'bg-info',
		),
		array(
			'title' => 'bg-warning',
			'selector' => 'p, a, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'bg-warning',
		),
		array(
			'title' => 'bg-danger',
			'selector' => 'p, a, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'bg-danger',
		),
		array(
			'title' => 'rightuppercase',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'rightuppercase',
		),
		array(
			'title' => 'leftuppercase',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'leftuppercase',
		),
		array(
			'title' => 'centeruppercase',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'centeruppercase',
		),
		array(
			'title' => 'pull-left',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'pull-left'
		),
		array(
			'title' => 'pull-right',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'pull-right'
		),
		array(
			'title' => 'img-rounded',
			'selector' => 'img',
			'classes' => 'img-rounded',
		),
		array(
			'title' => 'img-circle',
			'selector' => 'img',
			'classes' => 'img-circle',
		),
		array(
			'title' => 'img-thumbnail',
			'selector' => 'img',
			'classes' => 'img-thumbnail',
		),
		array(
			'title' => 'btn',
			'selector' => 'a, button',
			'classes' => 'btn',
		),
		array(
			'title' => 'btn-primary',
			'selector' => 'a, button',
			'classes' => 'btn-primary',
		),
		array(
			'title' => 'btn-success',
			'selector' => 'a, button',
			'classes' => 'btn-success',
		),
		array(
			'title' => 'btn-default',
			'selector' => 'a, button',
			'classes' => 'btn-default',
		),
		array(
			'title' => 'btn-info',
			'selector' => 'a, button',
			'classes' => 'btn btn-info',
		),
		array(
			'title' => 'btn-warning',
			'selector' => 'a, button',
			'classes' => 'btn-warning',
		),
		array(
			'title' => 'btn-danger',
			'selector' => 'a, button',
			'classes' => 'btn-danger',
		)		
	);
	$settings['style_formats'] = json_encode( $style_formats );
	return $settings;
} 
/*Learn TinyMCE style format options at http://www.tinymce.com/wiki.php/Configuration:formats*/
/*Add custom stylesheet to the website front-end with hook 'wp_enqueue_scripts'*/
add_action('wp_enqueue_scripts', 'tuts_mcekit_editor_enqueue'); 
/*Enqueue stylesheet, if it exists.*/
function tuts_mcekit_editor_enqueue() {
  $StyleUrl = get_stylesheet_directory_uri().'/css/editor-styles.css'; // Customstyle.css is relative to the current file
  wp_enqueue_style( 'myCustomStyles', $StyleUrl );
}
//change default media linking to nothing
update_option('image_default_link_type','none');
//Add Theme Options menu item to Admin Bar
function custom_toolbar_link($wp_admin_bar) {
	$args = array(
			'id' => 'of_theme_options',
			'title' => __( 'Theme Options' ),
			'href' => admin_url( 'themes.php?page=options-framework' )
		);
	$wp_admin_bar->add_node($args);
}
add_action('admin_bar_menu', 'custom_toolbar_link', 999);
wp_enqueue_script('jquery');
?>