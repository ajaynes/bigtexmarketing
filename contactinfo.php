<div class="contact-info">
	<div class="container">
  	<div class="row">
    	<div class="col-sm-5 in-touch">
      	<h2 class="leftunderline">Get in Touch</h2>
        <p>For more information about our Dealer Program, team members, please feel free to contact us today!</p>
      </div><!--col-sm-6-->
      <div class="col-sm-3 col-sm-offset-1 home-contact">
      	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/support-icon.png" class="img-responsive alignleft" />
        <h4 class="uppercase">Support</h4>
        <p>Is there a glitch in your system? Not a problem! <a href="#">Contact us</a></p>
      </div><!--col-sm-3-->
      <div class="col-sm-3">
      	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/demo-icon.png" class="img-responsive alignleft" />
        <h4 class="uppercase">Request a Demo</h4>
        <p>Want a sneak peek before you commit? <a href="#">Fill out the form</a></p>
      </div><!--col-sm-3-->
    </div><!--row-->
    <div class="contact-content">
      <div class="row">
        <div class="col-sm-6">
          <script type="text/javascript" src="https://bigtextrailers.formstack.com/forms/js.php/big_tex_marketing_contact?nojquery=1&no_style=1"></script><noscript><a href="https://bigtextrailers.formstack.com/forms/big_tex_marketing_contact" title="Online Form">Online Form - Big Tex Marketing Contact</a></noscript><script type='text/javascript'>if (typeof $ == 'undefined' && jQuery){ $ = jQuery}</script>
        </div>
        <div class="col-sm-6">
          <?php echo do_shortcode('[wpgmza id="1"]'); ?>
        </div>
      </div>
    </div>
  </div>
</div>