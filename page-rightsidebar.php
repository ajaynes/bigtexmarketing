<?php
/*
Template Name: Sidebar Right
*/
?>
<?php get_header(); ?>
<main class="content">
<?php get_template_part( 'slider' ); ?>
	<div class="row">
		<?php get_template_part( '/inc/parts/content', 'pageright' ); ?>
    <?php get_sidebar('right'); ?>
  </div><!--row-->
</main><!--content-->
<?php get_footer(); ?>